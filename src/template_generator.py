import numpy as np
import matplotlib.pylab as plt
import healpy as hp
from colossus.cosmology import cosmology
import scipy.interpolate as I

# define spherical data container
class sphere_array:
    def __init__(self, nr=10, dr=1,dx=1):
        self.nr     = nr
        self.dr     = dr
        self.dx     = dx
        self.fr     = 0.5
        self.alpha  = np.sqrt(np.pi/3.)
        self.x      = self.dr*(np.arange(nr)+1) #always choose upper bound of shell
        self.xmax   = np.max(self.x)*np.ones(len(self.x))
        
        
        #now choose resolution of healpix shells
        self.Nside = 2**(np.log2(self.alpha*self.xmax/self.fr/self.dx +1).astype(int)+1)
         
        #create python maps
        self.maps=[]
        for nside in self.Nside:
            self.maps.append(np.zeros(hp.pixelfunc.nside2npix(nside)))

        self.maps=np.array(self.maps)

#An isw generator a class
class template_generator:

    def __init__(self,ngrid=128,boxsize=512,qmin=np.array([-256,-256,-256])):
        self.description = "This isw_generator has not been described yet"
        self.author = "Jens Jasche"
        self.cosmo = cosmology.setCosmology('planck15')
        self.boxsize=boxsize
        self.ngrid=ngrid
        self.seed=314159
        self.rs = np.random.RandomState(self.seed)
        self.qmin=qmin
        self.speed_of_light = 299792.458 #km/s

    # ensuring arrays are hermitian
    def nyquist(self,xp):
        self.ngrid=xp.shape[0]
        xp[self.ngrid//2+1:,1:,0]= np.conj(np.fliplr(np.flipud(xp[1:self.ngrid//2,1:,0])))
        xp[self.ngrid//2+1:,0,0] = np.conj(xp[self.ngrid//2-1:0:-1,0,0])
        xp[0,self.ngrid//2+1:,0] = np.conj(xp[0,self.ngrid//2-1:0:-1,0])
        xp[self.ngrid//2,self.ngrid//2+1:,0] = np.conj(xp[self.ngrid//2,self.ngrid//2-1:0:-1,0])
    
        xp[self.ngrid//2+1:,1:,self.ngrid//2]= np.conj(np.fliplr(np.flipud(xp[1:self.ngrid//2,1:,self.ngrid//2])))
        xp[self.ngrid//2+1:,0,self.ngrid//2] = np.conj(xp[self.ngrid//2-1:0:-1,0,self.ngrid//2])
        xp[0,self.ngrid//2+1:,self.ngrid//2] = np.conj(xp[0,self.ngrid//2-1:0:-1,self.ngrid//2])
        xp[self.ngrid//2,self.ngrid//2+1:,self.ngrid//2] = np.conj(xp[self.ngrid//2,self.ngrid//2-1:0:-1,self.ngrid//2])
        return xp    
    
    def getPk(self,density, nkbins=64):
        #make sure the density has mean 0
        density=density-np.mean(density)
        ngrid=density.shape[0]
    
        #Fourier transform of density
        deltak=np.fft.rfftn(density)
    
        #Square the density in Fourier space to get the 3D power, make sure k=0 mode is 0
        dk2=(deltak*np.conjugate(deltak)).astype(np.float)
        dk2[0,0,0]=0.0
        
        k = np.fft.fftfreq(ngrid, d=self.boxsize/ngrid)*2*np.pi
        kgrid = np.sqrt(k[:,None,None]**2 + k[None,:,None]**2 + k[None,None,:self.ngrid//2+1]**2)
        kmin = 2*np.pi/np.float(self.boxsize)
        bins=np.logspace(np.log10(kmin), np.log10(np.max(kgrid)),nkbins)

        pnbin, bin_edges=np.histogram(kgrid, bins=bins)
        kkbin, bin_edges=np.histogram(kgrid,weights=kgrid, bins=bins)
        ppbin, bin_edges=np.histogram(kgrid,weights=dk2, bins=bins)
        
        foo=np.where(pnbin>0)
    
        pk    = ppbin[foo]/pnbin[foo]
        kmean = kkbin[foo]/pnbin[foo]
    
        pk*= self.boxsize**3/ngrid**6
    
        return kmean, pk
    
    def getS8(self,density):
        #make sure the density has mean 0
        density=density-np.mean(density)
        ngrid=density.shape[0]
    
        #Fourier transform of density
        deltak=np.fft.rfftn(density)
        
        k = np.fft.fftfreq(ngrid, d=self.boxsize/ngrid)*2*np.pi
        kgrid = np.sqrt(k[:,None,None]**2 + k[None,:,None]**2 + k[None,None,:self.ngrid//2+1]**2)
        
        kth = 2*np.pi/8.
        foo = np.where(kgrid>kth)
        deltak[foo]*=0.
        aux = np.fft.irfftn(deltak)
        return np.std(aux)
        
    
    def gen_gauss_field(self,exactpk=True, smw=0.0):
        kk = 10**np.arange(-5,2,0.02)
        pp = self.cosmo.matterPowerSpectrum(kk)
    
        thirdim=self.ngrid//2+1
        kmin = 2*np.pi/np.float(self.boxsize)
        sk = (self.ngrid,self.ngrid,thirdim)
    
        a = np.fromfunction(lambda x,y,z:x, sk).astype(np.float)
        a[np.where(a > self.ngrid//2)] -= self.ngrid
        b = np.fromfunction(lambda x,y,z:y, sk).astype(np.float)
        b[np.where(b > self.ngrid//2)] -= self.ngrid
        c = np.fromfunction(lambda x,y,z:z, sk).astype(np.float)
        c[np.where(c > self.ngrid//2)] -= self.ngrid
    
        kgrid = kmin*np.sqrt(a**2+b**2+c**2).astype(np.float)
    
        
    
        if (exactpk):
                dk = np.exp(2j*np.pi*self.rs.rand(self.ngrid*self.ngrid*(thirdim))).reshape((self.ngrid,self.ngrid,thirdim)).astype(np.complex64)
        else:
                dk = np.empty((self.ngrid,self.ngrid,thirdim),dtype=N.complex64)
                dk.real = self.rs.normal(size=self.ngrid*self.ngrid*(thirdim)).reshape((self.ngrid,self.ngrid,thirdim)).astype(np.float32)
                dk.imag = self.rs.normal(size=self.ngrid*self.ngrid*(thirdim)).reshape((self.ngrid,self.ngrid,thirdim)).astype(np.float32)
                dk /= np.sqrt(2.0)
    
        filt=np.exp(-kgrid*kgrid*smw*smw)
    
        
    
        pkinterp=I.interp1d(kk, pp)
     
        if (kgrid[0,0,0]==0):
                dk[0,0,0]=0
                wn0=np.where(kgrid!=0)
        
                dk[wn0] *= np.sqrt(filt[wn0]*pkinterp(kgrid[wn0]))*self.ngrid**3/self.boxsize**1.5
        else:
                dk *= np.sqrt(filt*pkinterp(kgrid.flatten())).reshape(sk)*self.ngrid**3/self.boxsize**1.5
                
        dk=self.nyquist(dk)
        dens = np.fft.irfftn(dk)
        return dens

    def get_phi(self,dens):

        print(np.min(dens),np.max(dens))

        densft = np.fft.rfftn(dens)
        print(np.min(densft),np.max(densft))
        
        k = np.fft.fftfreq(self.ngrid, d=self.boxsize/self.ngrid)*2*np.pi
        kgrid = np.sqrt(k[:,None,None]**2 + k[None,:,None]**2 + k[None,None,:self.ngrid//2+1]**2)
       
        #check factors of 'h' !!!!!!!!!!
        #calculate gravitational potential at redshift z =0
        fac = (3./2.)*self.cosmo.Om0* (self.cosmo.H0)**2
        
        phi = -fac*(densft)/(kgrid**2)
        phi[0,0,0]=0.
        return np.fft.irfftn(phi) # [km**2/s**2]
    
    def growth_function(self,r_com):
        #input r = array of comoving distances
        z=np.linspace(0.0,10,20000)
        
        #now calculate the isw kernel
        gf = self.cosmo.growthFactor(z)
        
        dcom=self.cosmo.comovingDistance(z_min=0.0, z_max=z, transverse=True)
        
        gf_interp=I.interp1d(dcom, gf)
        
        return gf_interp(r_com)
        
    
    
    def isw_kernel(self,r_com):
        #input r = array of comoving distances
        z=np.linspace(0.0,10,20000)
        
        #now calculate the isw kernel
        kernel_isw = -(self.cosmo.growthFactor(z, derivative=1)*(1+z)+self.cosmo.growthFactor(z))*self.cosmo.Hz(z)*(1.+z) # [km/s/Mpc]
        kernel_isw *= 2./self.speed_of_light**2*self.cosmo.Tcmb0 #prefactor
        kernel_isw *= 1./(self.speed_of_light*(1+z)) #differential
        dcom=self.cosmo.comovingDistance(z_min=0.0, z_max=z, transverse=True)
        
        kernel_isw_interp=I.interp1d(dcom, kernel_isw)
        
        return kernel_isw_interp(r_com)
    
    def convergence_kernel(self,r_com, build_light_cone= True):
        #input r = array of comoving distances
        z=np.linspace(0.0,10,20000)
        
        #now calculate the isw kernel
        kernel_k = (3./2.)*self.cosmo.Om0* (self.cosmo.H0)**2 /self.speed_of_light**2 * (1.+z) # [1/Mpc**2]
        
        #only apply growth function if density field is not on light cone
        if(build_light_cone):
            kernel_k*=self.cosmo.growthFactor(z)
        
        dcom=self.cosmo.comovingDistance(z_min=0.0, z_max=z, transverse=True)
        
        kernel_k_interp=I.interp1d(dcom, kernel_k)
        
        return kernel_k_interp(r_com)
    
    def time_delay_kernel(self,r_com):
        #input r = array of comoving distances
        z=np.linspace(0.0,10,20000)
        
        #now calculate the isw kernel
        kernel_td = 2/self.speed_of_light**3 * self.cosmo.growthFactor(z) # [s**2/km**2]
        dcom=self.cosmo.comovingDistance(z_min=0.0, z_max=z, transverse=True)
        
        kernel_td_interp=I.interp1d(dcom, kernel_td)
        
        return kernel_td_interp(r_com)
    
    def gen_isw(self,sph_phi):
        
        R      = sph_phi.x  #always choose upper bound of shell
        
        isw_kernel = self.isw_kernel(R)
        
        #now do integral
        Ru=0
        Rl=0
        isw=np.zeros(np.shape(sph_phi.maps[0]))
        for i in np.arange(len(sph_phi.maps)):
            Ru=R[i]
            dr = Ru-Rl
            isw+=sph_phi.maps[i]*isw_kernel[i]*dr
            Rl = Ru
        
        return isw
    
    def projector_lowmem(self,field):
        
        Dx=self.boxsize/float(self.ngrid)
        #find smallest radius fully contained in the box
        R  = np.sqrt((self.boxsize+self.qmin[0])**2)
        aux = np.sqrt((self.boxsize+self.qmin[1])**2)
        if(aux<R):
            R=aux
        aux = np.sqrt((self.boxsize+self.qmin[2])**2)
        if(aux<R):
            R=aux
            
        print('TEST R', R)    
        
        
        #1) guess minimal number of los elements N
        DR = Dx #we want to have 1/fR samples per voxel
        NR = int(R/DR) - 1
        
        #2)
        fR = 4 #we want to oversample the los by a factor of fR
        NR*=fR
        
        #3) choose propper differential dR
        DR = R/float(NR)
        
        #4) decide pixel size for healpix (e.g. we want to resolve the voxels at the largest distance R)
        fr     = 0.5
        alpha  = np.sqrt(np.pi/3.)
        Nside  = 2**(np.log2(alpha*R/fr/Dx +1).astype(int)+1)
        Npix   = hp.pixelfunc.nside2npix(Nside)
        
        #5) prepare projection
        ipix=np.arange(Npix)
        dx,dy,dz=hp.pix2vec(Nside, ipix)
        d = np.sqrt(dx * dx + dy * dy + dz * dz)
        dx = dx / d; dy = dy / d; dz = dz / d # ray unit vector
    
        #build map in galactic coordinates
        rot = hp.Rotator(coord=['G','C'])
        dx,dy,dz = rot(dx,dy,dz)
        
        res=0.
        fl=np.zeros(Npix)
        
        Xl = 0*dx-self.qmin[0]; Yl = 0*dy-self.qmin[1]; Zl = 0*dz-self.qmin[2]
        
        #find nearest grid point
        Xl /= Dx; Yl /= Dx; Zl /= Dx 
        
        ix = (np.rint(Xl[0])).astype(int)
        iy = (np.rint(Yl[0])).astype(int)
        iz = (np.rint(Zl[0])).astype(int)
        
        if( (ix > -1)*(ix < self.ngrid)*(iy > -1)*(iy < self.ngrid)*(iz > -1)*(iz < self.ngrid)):
        
            jx = (ix+1) % self.ngrid;
            jy = (iy+1) % self.ngrid;
            jz = (iz+1) % self.ngrid;
            rx = (Xl[0] - ix);
            ry = (Yl[0] - iy);
            rz = (Zl[0] - iz);
            qx = 1.-rx;
            qy = 1.-ry;
            qz = 1.-rz;
            
            fl+=field[ix,iy,iz] * qx * qy * qz +field[ix,iy,jz] * qx * qy * rz +field[ix,jy,iz] * qx * ry * qz +field[ix,jy,jz] * qx * ry * rz +field[jx,iy,iz] * rx * qy * qz +field[jx,iy,jz] * rx * qy * rz +field[jx,jy,iz] * rx * ry * qz +field[jx,jy,jz] * rx * ry * rz;
    
        fu=np.zeros(Npix)
        for i in np.arange(NR):
            Ru = DR*(i+1)
            Rl = DR*(i)
            
            Xu = Ru*dx-self.qmin[0]; Yu = Ru*dy-self.qmin[1]; Zu = Ru*dz-self.qmin[2]
            
            #find nearest grid point
            Xu /= Dx; Yu /= Dx; Zu /= Dx 
        
            ix = (np.rint(Xu)).astype(int)
            iy = (np.rint(Yu)).astype(int)
            iz = (np.rint(Zu)).astype(int)
        
            foo=np.where( (ix > -1)*(ix < self.ngrid)*(iy > -1)*(iy < self.ngrid)*(iz > -1)*(iz < self.ngrid))

            ix = ix[foo]
            iy = iy[foo]
            iz = iz[foo]
        
            jx = (ix+1) % self.ngrid;
            jy = (iy+1) % self.ngrid;
            jz = (iz+1) % self.ngrid;
            rx = (Xu[foo] - ix);
            ry = (Yu[foo] - iy);
            rz = (Zu[foo] - iz);
            qx = 1.-rx;
            qy = 1.-ry;
            qz = 1.-rz;
            
            fu*=0
        
            #tri-linear interpolation
            fu[foo] = field[ix,iy,iz] * qx * qy * qz +field[ix,iy,jz] * qx * qy * rz +field[ix,jy,iz] * qx * ry * qz +field[ix,jy,jz] * qx * ry * rz +field[jx,iy,iz] * rx * qy * qz +field[jx,iy,jz] * rx * qy * rz +field[jx,jy,iz] * rx * ry * qz +field[jx,jy,jz] * rx * ry * rz; 
            res+=0.5*(fu+fl)*DR
            fl=np.copy(fu)
        
        return res
    
    def gen_isw_lowmem(self,dens, build_light_cone= True):
        
        #1) setup grid
        r = np.arange(self.ngrid)*self.boxsize/float(self.ngrid)
        rgrid = np.sqrt((r[:,None,None]+self.qmin[0])**2 + (r[None,:,None]+self.qmin[1])**2 + (r[None,None,:]+self.qmin[2])**2)
        
        #2) build potential
        
        if(build_light_cone==False):
            #we need to undo the light-cone calculation of BORG before calculating the potential
            gf= self.growth_function(rgrid.flatten()).reshape((self.ngrid,self.ngrid,self.ngrid))
            dens/=gf
            
        phi=self.get_phi(dens)
        
        #3) build 3d isw kernel
        isw_kernel = self.isw_kernel(rgrid.flatten()).reshape((self.ngrid,self.ngrid,self.ngrid))
        
        #4) multiply kernel with potential
        phi*=isw_kernel
        
        #5) calculate the projection
        T_ISW=self.projector_lowmem(phi)
        
        return T_ISW

    def gen_convergence_lowmem(self,dens,zs=1100., build_light_cone=True):
        
        rs=self.cosmo.comovingDistance(z_min=0.0, z_max=zs, transverse=True)
        
        #1) build 3d isw kernel
        r = np.arange(self.ngrid)*self.boxsize/float(self.ngrid)
        rgrid = np.sqrt((r[:,None,None]+self.qmin[0])**2 + (r[None,:,None]+self.qmin[1])**2 + (r[None,None,:]+self.qmin[2])**2)
        
        #2) estimate cosmology part of kernel
        k_kernel = self.convergence_kernel(rgrid.flatten(), build_light_cone= build_light_cone).reshape((self.ngrid,self.ngrid,self.ngrid))
        
        #3) estimate shape function
        k_kernel *= (rs - r) * r / rs
        
        #3) multiply kernel with potential
        kappa3d = k_kernel * dens
        
        #4) calculate the projection
        kappa=self.projector_lowmem(kappa3d)
        
        return kappa
    
    def gen_time_delay_lowmem(self,dens,zs=1100.):
        
        rs=self.cosmo.comovingDistance(z_min=0.0, z_max=zs, transverse=True)
        
        #1) build potential
        phi=self.get_phi(dens)

        print(np.max(phi),np.min(phi))
        
        #2) build 3d td kernel
        r = np.arange(self.ngrid)*self.boxsize/float(self.ngrid)
        rgrid = np.sqrt((r[:,None,None]+self.qmin[0])**2 + (r[None,:,None]+self.qmin[1])**2 + (r[None,None,:]+self.qmin[2])**2)
        

        #np.average()	
        #plt.scatter((rgrid.flatten())[::1000],(phi.flatten())[::1000])
        #plt.show()

        td_kernel = self.time_delay_kernel(rgrid.flatten()).reshape((self.ngrid,self.ngrid,self.ngrid))
        
        foo=np.where(rgrid > rs)
        
        #3) multiply kernel with potential
        phi*=td_kernel
        
        #only integrate to maximum redshift
        phi[foo]*=0.
        
        #4) calculate the projection
        TD=self.projector_lowmem(phi)
        
        return TD*3.0861e19 #the last factor accounts for the conversion of dr[Mpc] to dr[km]​
    
    def proj_cart2sph(self,field):
        ngrid = np.shape(field)[0]
    
        Dx=self.boxsize/float(ngrid)
        #choose radial stepping
        
        #find smallest radius fully contained in the box
        R  = np.sqrt((self.boxsize+self.qmin[0])**2)
        aux = np.sqrt((self.boxsize+self.qmin[1])**2)
        if(aux<R):
            R=aux
        aux = np.sqrt((self.boxsize+self.qmin[2])**2)
        if(aux<R):
            R=aux
        R=R-Dx*0.5
        fr=0.5 # we want to have a finer binning than the voxel size
        nr = int(R/(0.5*Dx*fr))   
        dr = R/float(nr+1)
        
        print (R, (nr+1)*dr)
    
        #define spherical array
        #sph=sphere_array(nr=nr,dr=dr,dx=Dx)
        sph=sphere_array(nr=nr,dr=dr,dx=Dx)
    
        nside   = sph.Nside[0]
        ipix=np.arange(hp.pixelfunc.nside2npix(nside))
        dx,dy,dz=hp.pix2vec(nside, ipix)
        d = np.sqrt(dx * dx + dy * dy + dz * dz)
        dx = dx / d; dy = dy / d; dz = dz / d # ray unit vector
    
        #build map in galactic coordinates
        rot = hp.Rotator(coord=['G','C'])
        dx,dy,dz = rot(dx,dy,dz)
    
        Rl = 0.
        Ru = 0.
        for i in np.arange(sph.nr):
        
                Ru = sph.x[i] #upper radius of shell
        
                Rc = 0.5*(Ru+Rl) #calculate center radius of shell
                Rl = Ru
          
                X = Rc*dx-self.qmin[0]; Y = Rc*dy-self.qmin[1]; Z = Rc*dz-self.qmin[2]
        
                #find nearest grid point
                X /= Dx; Y /= Dx; Z /= Dx 
        
                ix = (np.rint(X)).astype(int)
                iy = (np.rint(Y)).astype(int)
                iz = (np.rint(Z)).astype(int)
        
                foo=np.where( (ix > -1)*(ix < self.ngrid)*(iy > -1)*(iy < self.ngrid)*(iz > -1)*(iz < self.ngrid))

                ix = ix[foo]
                iy = iy[foo]
                iz = iz[foo]
        
                jx = (ix+1) % self.ngrid;
                jy = (iy+1) % self.ngrid;
                jz = (iz+1) % self.ngrid;
                rx = (X[foo] - ix);
                ry = (Y[foo] - iy);
                rz = (Z[foo] - iz);
                qx = 1.-rx;
                qy = 1.-ry;
                qz = 1.-rz;
        
                sph.maps[i]*=np.nan
        
                #tri-linear interpolation
                sph.maps[i][foo] = field[ix,iy,iz] * qx * qy * qz +field[ix,iy,jz] * qx * qy * rz +field[ix,jy,iz] * qx * ry * qz +field[ix,jy,jz] * qx * ry * rz +field[jx,iy,iz] * rx * qy * qz +field[jx,iy,jz] * rx * qy * rz +field[jx,jy,iz] * rx * ry * qz +field[jx,jy,jz] * rx * ry * rz; 
      
        return sph      
    
