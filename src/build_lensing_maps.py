import numpy as np
from src.template_generator import *
import h5py


def change_coord(m, coord):
    """ Change coordinates of a HEALPIX map

    Parameters
    ----------
    m : map or array of maps
      map(s) to be rotated
    coord : sequence of two character
      First character is the coordinate system of m, second character
      is the coordinate system of the output map. As in HEALPIX, allowed
      coordinate systems are 'G' (galactic), 'E' (ecliptic) or 'C' (equatorial)

    Example
    -------
    The following rotate m from galactic to equatorial coordinates.
    Notice that m can contain both temperature and polarization.
    >>>> change_coord(m, ['G', 'C'])
    """
    # Basic HEALPix parameters
    npix = m.shape[-1]
    nside = hp.npix2nside(npix)
    ang = hp.pix2ang(nside, np.arange(npix))

    # Select the coordinate transformation
    rot = hp.Rotator(coord=reversed(coord))

    # Convert the coordinates
    new_ang = rot(*ang)
    new_pix = hp.ang2pix(nside, *new_ang)

    return m[..., new_pix]


def build_maps_isw(bsize=4000.,ngrid=256,qmin=np.array([-2200.0,-2000.0,-300.0]),filenames=["f1.dat","f2.dat","f3.dat"],random=True):
    
    tg=template_generator(ngrid=ngrid,boxsize=bsize,qmin=qmin)
    
    isw=[]
    print('preparing maps...')  

    if(random == True):
      s=0 
      for fname in filenames:
        print('generate random theory sample nr:',s)
        dens=tg.gen_gauss_field()
        isw.append(tg.gen_isw_lowmem(dens, build_light_cone= True))
        np.savez('theory_isw_maps',isw=np.array(isw))
        s+=1
    else:
      for fname in filenames:
        print('processing borg mcmc file:',fname)
        f = h5py.File(fname, 'r')
        delta_final = f['scalars']['BORG_final_density'][...]
        isw.append(tg.gen_isw_lowmem(delta_final, build_light_cone=False))
        np.savez('borg_isw_maps',isw=np.array(isw))     
  

    print('preparing maps done...') 







def build_maps_lensing(bsize=4000.,ngrid=256,qmin=np.array([-2200.0,-2000.0,-300.0]),filenames=["f1.dat","f2.dat","f3.dat"],random=True):
    
    tg=template_generator(ngrid=ngrid,boxsize=bsize,qmin=qmin)

    kappa=[]
    print('preparing maps...')  

    if(random == True):
      s=0 
      for fname in filenames:
        print('generate random theory sample nr:',s)
        dens=tg.gen_gauss_field()
        kappa.append(tg.gen_convergence_lowmem(dens))
        np.savez('theory_kappa_maps',kappa=np.array(kappa))
        s+=1
    else:
      for fname in filenames:
        print('processing borg mcmc file:',fname)
        with h5py.File(fname, 'r') as ff:
          delta_final = ff['scalars']['BORG_final_density'][...]
        kappa.append(tg.gen_convergence_lowmem(delta_final, build_light_cone=False))
        np.savez('borg_kappa_maps',kappa=np.array(kappa))     
  

    print('preparing maps done...') 

def build_maps_lensing_resim(bsize=4000.,ngrid=256,qmin=np.array([-2200.0,-2000.0,-300.0]),filenames=["f1.dat","f2.dat","f3.dat"],random=True):
    
    tg=template_generator(ngrid=ngrid,boxsize=bsize,qmin=qmin)

    kappa=[]
    print('preparing maps...')  

    if(random == True):
      s=0 
      for fname in filenames:
        print('generate random theory sample nr:',s)
        dens=tg.gen_gauss_field()
        kappa.append(tg.gen_convergence_lowmem(dens))
        np.savez('theory_kappa_maps_resim',kappa=np.array(kappa))
        s+=1
    else:
      for fname in filenames:
        print('processing borg mcmc file:',fname)
        f = np.load(fname) #this is the density
        meanf= np.sum(f)/np.shape(f)[0]**3
        delta_final = f/meanf-1.
        kappa.append(tg.gen_convergence_lowmem(delta_final, build_light_cone=False))
        np.savez('borg_kappa_maps_resim',kappa=np.array(kappa))     
  

    print('preparing maps done...')     
    
def build_maps_time_delay(zs,bsize=4000.,ngrid=256,qmin=np.array([-2200.0,-2000.0,-300.0]),filenames=["f1.dat"],random=True):
    
    tg=template_generator(ngrid=ngrid,boxsize=bsize,qmin=qmin)

    td=[]
    print('preparing maps...')  

    if(random == True): 
      s=0
      for fname in filenames:
        print('generate random theory sample nr:',s)
        dens=tg.gen_gauss_field()
        td.append(tg.gen_time_delay_lowmem(dens,zs))
        np.savez('theory_time_delay_maps',td=np.array(td))
        s+=1
    else:
      for fname in filenames:
        print('processing borg mcmc file:',fname)
        f = h5py.File(fname, 'r')
        delta_final = f['scalars']['BORG_final_density'][...]
        td.append(tg.gen_time_delay_lowmem(delta_final,zs))
        np.savez('borg_time_delay_maps',td=np.array(td))      
  

    print('preparing maps done...')
    
    
    

